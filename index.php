<?php
require "classes/Url.php";
?><!DOCTYPE >
<html xmlns="" xml:lang="pt-br">
<head>
    <title>sheep</title>


    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
    <meta name="author" content="Layout: Mauricio Samy Silva / Código: Eduardo Kraus" /> 
    <link rel="stylesheet" type="text/css"  media="screen,projection" href="<?php echo URL::getBase() ?>estilo/materialize/css/materialize.min.css"/>  
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" type="text/css"  media="screen,projection" href="<?php echo URL::getBase() ?>estilo/normalize/normalize.css"/>
    <link rel="stylesheet" type="text/css"  media="screen,projection" href="<?php echo URL::getBase() ?>estilo/style.css"/>

</head>
<body>


    <?php
    $modulo = Url::getURL( 0 );

    if( $modulo == null )
        $modulo = "home";

    if( file_exists( "modulos/" . $modulo . ".php" ) )
        require "modulos/" . $modulo . ".php";
    else
        require "modulos/404.php";
    ?>


    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/materialize/js/materialize.min.js"></script>
    <script type="text/javascript">
      $(document).ready(function(){
         $(".button-collapse").sideNav();
         $('.parallax').parallax();

$('.efeito-desliza').click(function(){
$('html, body').animate({
scrollTop: $( $(this).attr('href') ).offset().top
}, 2500);
return false;
});

     });



 </script>


 
</body>
</html>