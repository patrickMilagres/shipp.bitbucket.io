
<div class="row">
	<div class="mod1 col s12 m9 z-depth-3">
		<div class="parallax-container">
			<div class="parallax"><img src="<?php echo URL::getBase() ?>imagens/img1-home.png">


			</div>
			<div class="row text-mod1 ">
				<div class="col s12 ">
					<p>Sem chefe,<br>Sem escritório.<p>
						<p>Voce no controle<br> do seu trabalho.</p>

					</div>
				</div>
			</div>

		</div>
		<nav class="transparent col s12 m9">      
			<div class="nav-wrapper ">       
				<div class="col s4 m2 l2 left-align " id="logoshiip" title="shipp">
					<a href="#!" class="logo"><img src="<?php echo URL::getBase() ?>imagens/logo.png"></a>
					<a href="#!" class="logo2"><img src="<?php echo URL::getBase() ?>imagens/logo2.png"></a>

				</div>
				<a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
				<ul class="right hide-on-med-and-down">
					<li><a href="collapsible.html">contato</a></li>
					<li><a href="mobile.html">perguntas?</a></li>
				</ul>
				<ul class="side-nav" id="mobile-demo">  
					<li><a href="collapsible.html">contato</a></li>
					<li><a href="mobile.html">perguntas?</a></li>
				</ul>
			</div>
		</nav>

		<div class="row">
			<div class="mod1 col s12 m3 z-depth-3"><div class="mod1-2-home">  

				<div class="container ">
					<div class="row link">
						<div class="col s12 center-align "><a href="">Cadastrar</a></div>
					</div>

					<div class="mod1-cadastro row ">
						<form class="col s12">

							<div class="row">
								<div class="input-field col s12">
									<input  id="first_name" type="text" class="validate">
									<label >nome</label>
								</div>
							</div>
							<div class="row">
								<div class="input-field col s12">
									<input  id="first_name" type="text" class="validate">
									<label>Telefone</label>
								</div>
							</div>

							<div class="row">
								<div class="input-field col s12">
									<input id="email" type="email" class="validate">
									<label >Email</label>
								</div>
							</div>
							<div class="row">
								<div class="col s12">
									<div class="input-field inline">
										<input id="email" type="email" class="validate">
										<label for="email" data-error="wrong" data-success="right">Email</label>
									</div>
								</div>
							</div>
							<div class="col s12 center-align"><button type="button" class="btn btn-default">avançar</button></div>
						</form>
					</div>   
				</div>
			</div>	
		</div>

		<div id="ancora scroll" class="row-mod2 row">
			<div class="col s12 mod2-parallax ">
				<div class=" parallax-container " style="padding: 0px">
					<div class="parallax"><img src="<?php echo URL::getBase() ?>imagens/img-mod2.png" class="img-mod2">
					</div>
					<div class="row text-mod2 ">
						<div class="row">
							<div class="col s12 ">
								<p>Ta procurando uma renda extra?<br>Vem para o nosso time!<p>
									<p class="p2">Com a Shipp isso é possivel, aqui voce faz o seu horário sem<br>se preocupar com a carga horaria. Baixa o app e comece a<br> entregar logo!!</p>
								</div>
							</div>

							<div class="row">
								<div class="col s12 m4 img-footer ">
									<a href=""> <img src="<?php echo URL::getBase() ?>imagens/img-googleplay.png"></a>
								</div>
								<div class="col s12 m4 img-footer ">
									<a href=""> <img src="<?php echo URL::getBase() ?>imagens/img-appStore.png"></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				




				<div class="row ">
					<div class="col s12">
						<div class="row txt-mod2-mobie">
						<div class="row">
							<div class="col s12 ">
								<p>Ta procurando uma renda extra?<br>Vem para o nosso time!<p>
									<p class="p2">Com a Shipp isso é possivel, aqui voce faz o seu horário sem<br>se preocupar com a carga horaria. Baixa o app e comece a<br> entregar logo!!</p>
								</div>
							</div>

							<div class="row">
								<div class="col s5 img-footer-mobie ">
									<a href=""> <img src="<?php echo URL::getBase() ?>imagens/img-googleplay.png"></a>
								</div>
								<div class="col s5 img-footer-mobie ">
									<a href=""> <img src="<?php echo URL::getBase() ?>imagens/img-appStore.png"></a>
								</div>
							</div>
						</div>

						</div>
					</div>













				</div>
			</div>


			<footer class="page-footer ">
				<div class="container">
					<div class="row">
						<div class="col l3 s12">
							<p class="grey-text text-lighten-4"><content><a class="grey-text text-lighten-3" href="#!">Sobre a Shipp</a></content></p>
							<p class="grey-text text-lighten-4"><content><a class="grey-text text-lighten-3" href="#!">Quero ser entregador</a></content></p>
							<p class="grey-text text-lighten-4"><content><a class="grey-text text-lighten-3" href="#!">Quero ser Parceiro</a></content></p>
						</div>
						<div class="col l3 s12">
							<p class="grey-text text-lighten-4"><content><a class="grey-text text-lighten-3" href="#!">Ver clientes</a></content></p>
							<p class="grey-text text-lighten-4"><content><a class="grey-text text-lighten-3" href="#!">Ajuda</a></content></p>
							<p class="grey-text text-lighten-4"><content><a class="grey-text text-lighten-3" href="#!">Perguntas frequentes</a></content></p>
						</div>
						<div class="col l3 s12 img-footer ">
							<a href=""> <img src="<?php echo URL::getBase() ?>imagens/img-googleplay.png"></a>   
						</div>
						<div class="col l3 s12 img-footer ">
							<a href=""><img src="<?php echo URL::getBase() ?>imagens/img-appStore.png"></a>
						</div>
					</div>
				</div>
				<div class="footer-copyright">
					<div class="container">
						© 2014 Copyright Text
					</div>
				</div>
			</footer>
